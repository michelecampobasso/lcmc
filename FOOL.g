grammar FOOL;
 
@header{
import java.util.ArrayList;
import java.util.HashMap;
import ast.*;
import lib.FOOLlib;
}

@lexer::members {
int lexicalErrors=0;
}

@members{
  private ArrayList<HashMap<String, STentry>>  symTable = new ArrayList<HashMap<String, STentry>>();
 /*
  * classTable: 
  *   mappa nomi di classi in informazioni sugli elementi dichiarati al suo interno ed ereditati che serve per:
  *     - preservare le dichiarazioni interne ad una classe una volta che il parser ha concluso la dichiarazione di una classe
  *     - renderle accedibili anche in seguito tramite il nome della classe (es calcolo Virtual Table)
  *     - mantenere in forma aggregata le info di dichiarazione degli elementi di una classe e di      
  *     quelle da cui eredita al netto dell'overriding (elementi virtuali, es la Virtual Table)
  */
  private HashMap<String, CTentry> classTable = new HashMap<String, CTentry>();
  private HashMap<String, String> superType = new HashMap<String, String>();
  private int nestingLevel = -1;
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

prog	returns [Node ast]
	      :       
	      e=exp SEMIC	
        {
          /*
           * Inizio del parsing: fornisco a FOOLlib sin da subito il riferimento
           * alla tabella che contiene le relazioni tra i sottotipi ed i loro supertipi. 
           * Introdotto con l'estensione OO.
           */
          $ast = new ProgNode($e.ast);
          FOOLlib.putSuperType(superType); 
        }
        | LET 
        { 
          nestingLevel++;
          HashMap<String, STentry> hm = new HashMap<String, STentry>();
          symTable.add(hm);
        }
        // cllist introdotto con l'estensione OO
        c=cllist d=declist 
        IN e=exp SEMIC 
        { 
          symTable.remove(nestingLevel--);
          $ast = new ProgLetInNode($c.astlist, $d.astlist, $e.ast); 
          FOOLlib.putSuperType(superType); 
        } 
	;
	
/*
 * Estensione OO: la nuova variabile del linguaggio cllist serve a parsare la struttura di una classe.
 */
cllist returns [ArrayList<Node> astlist]
        :     
        { 
          $astlist = new ArrayList<Node>(); 
        }
  
        (CLASS i=ID 
          { 
            /*
             * Creo una CTentry vergine, ottengo il nestingLevel (dovrebbe sempre essere 0
             * da specifiche sull'assenza di dichiarazione di classi innestate),
             * introduco la nuova entry per l'AST e tento di aggiungerla. In caso positivo,
             * creo il nodo della classe corrispondente associandone nome e CTentry.
             */
            CTentry currentClass = new CTentry($i.text); 
            HashMap<String,STentry> hm = symTable.get(nestingLevel);
            // Costruttore STentry senza tipo per le classi (default null)
            STentry entry = new STentry(nestingLevel, 0); //una nuova classe ha sempre offset 0
            if (hm.put($i.text,entry)!=null) { 
              System.out.println("Class id "+$i.text+" at line "+$i.line+" already declared");
              System.exit(0);
            }
            ClassNode clNode = new ClassNode($i.text, currentClass, null);
          }
          (EXTENDS ei=ID 
          {
            /*
             * Se la classe introdotta � una sottoclasse, prima controllo che questa sia stata gi�
             * dichiarata. Se esiste, aggiungo alla tabella delle relazioni supertipo e sottotipo
             * quella appena individuata. Dunque procedo ad ereditare metodi e campi della superclasse 
             * (ottengo anche la VirtualTable corrispondente) e creo una CTentry padre per mantenerne 
             * il riferimento. Aggiorno dunque il classNode aggiungendogli il riferimento al padre.
             */
            if (classTable.get($ei.text)==null) {
              System.out.println("Class id "+$ei.text+" at line "+$ei.line+" not declared");
              System.exit(0);
            }
            superType.put($i.text, $ei.text);
            currentClass = (CTentry)classTable.get($ei.text).clone();
            currentClass.setId($i.text); // Il contenuto � identico a quello della classe ereditata, ma il nome va aggiornato
            CTentry superClass= classTable.get($ei.text);
            clNode = new ClassNode($i.text, currentClass, superClass);
          }
          )? LPAR
          {
            /*
             * Solo dopo aver parsato le prime informazioni della classe, posso procedere ad aggiungerle
             * alle relative strutture dati ed entro nello scope. In particolare, aggiungo alla cllist
             * il clNode appena prodotto e alla classTable la CTentry rappresentativa. Inoltre ricavo la virtualTable 
             * della classe corrente (vuota se non eredita, popolata altrimenti) e la aggiungo alla symbolTable.
             */
            $astlist.add(clNode); 
            classTable.put($i.text, currentClass);
            nestingLevel++;
            HashMap<String,STentry> hmn = currentClass.getVirtualTable();
            symTable.add(hmn);
          } 
          (ffi=ID COLON fft=basic 
          {
            /*
             * Comincio a catturare i campi uno alla volta coi rispettivi tipi e li
             * aggiungo alla entry corrente della classTable e del classNode.
             */
            Node field = new FieldNode($ffi.text, $fft.ast);
            currentClass.addField(field);
            clNode.addField(field);
          }
          (COMMA fi=ID COLON ft=basic
          {
            /*
             * Proseguo analogamente a sopra.
             */
            field = new FieldNode($fi.text, $ft.ast);
            currentClass.addField(field);
            clNode.addField(field);
          })* )? RPAR    
          CLPAR //Entro nel corpo della classe
          (FUN mi=ID COLON mt=basic
          {
            /*
             * A questo punto incontro solamente dichiarazioni dei metodi e, per ognuno di essi,
             * creo un nuovo nodo, aggiungo una STentry rappresentante il metodo alla CTentry della
             * classe corrente e mi sposto nello scope del metodo. 
             * Inizializzo inoltre sia l'array delle dichiarazioni, che quello dei parametri, in modo
             * da non incombere in NullPointerExceptions anche in caso di assenza di questi.
             */
            MethodNode methodNode =  new MethodNode($mi.text, $mt.ast);
            STentry metSTentry = currentClass.addMethod(methodNode);
            nestingLevel++;
            hmn = new HashMap<String,STentry>();
            symTable.add(hmn);  
            ArrayList<Node> parTypes = new ArrayList<Node>();
            int offsetParameters = 1;
            ArrayList<Node> declarations = new ArrayList<Node>(); 
            int offsetDeclarations = -2;
          }
          LPAR (fpi=ID COLON fpt=type 
          {
            /*
             * Per ogni parametro, creo il nodo che lo rappresenta e lo aggiungo al methodNode.
             * Come per i parametri delle funzioni, qualora il tipo sia un ArrowType, verr�
             * riservato un doppio spazio per la costruzione dell'AR.
             */
            parTypes.add($fpt.ast); 
            ParNode fpar = new ParNode($fpi.text, $fpt.ast);
            methodNode.addPar(fpar);
            if($fpt.ast instanceof ArrowTypeNode)
              offsetParameters++;
            if (hmn.put($fpi.text, new STentry(nestingLevel, $fpt.ast, offsetParameters++)) != null)
            {
              System.out.println("Parameter id "+$fpi.text+" at line "+$fpi.line+" already declared");
              System.exit(0);
            }
          }
          (COMMA pi=ID COLON pt=type
          {
            /*
             * Analogo a sopra.
             */
            parTypes.add($pt.ast); 
            ParNode par = new ParNode($pi.text, $pt.ast);
            methodNode.addPar(par);
            if($pt.ast instanceof ArrowTypeNode)
              offsetParameters++;
            if (hmn.put($pi.text, new STentry(nestingLevel, $pt.ast, offsetParameters++)) != null)
            {
              System.out.println("Parameter id "+$pi.text+" at line "+$pi.line+" already declared");
              System.exit(0);
            }
          })* )? RPAR
          {
            /*
             * Una volta raccolti il tipo di ritorno del metodo ed i suoi parametri,
             * � possibile creare l'ArrowTypeNode che lo rappresenta ed associarlo
             * sia alla corrispondente STentry, che al methodNode. 
             */
            ArrowTypeNode type = new ArrowTypeNode(parTypes, $mt.ast);
            metSTentry.addType(type);
            methodNode.addSymType(type);
            clNode.addMethod(methodNode);
          }
          (LET (VAR i=ID COLON b=basic ASS e=exp SEMIC
          {
            /* Let In: per ogni dichiarazione viene creato un VarNode e viene
             * aggiunto alla symbolTable.
             */
	          declarations.add(new VarNode($i.text, $b.ast, $e.ast));
	          if (hmn.put($i.text,new STentry(nestingLevel,$b.ast,offsetDeclarations--)) != null)
            {
              System.out.println("Local variable "+$i.text+" at line "+$i.line+" already declared");
              System.exit(0);
            }
            /*if ($t.ast instanceof ArrowTypeNode)
              offsetDeclarations--;*/
              //NON DEVO CONTROLLARLO ANCHE QUI?
          })* IN)? body=exp SEMIC 
          { 
            /* 
             * Il parsing del metodo � terminato: posso procedere ad aggiungere al 
             * methodNode la lista delle dichiarazioni ed il corpo del metodo.
             * Dal momento che sto uscendo dallo scope, rimuovo la testa della tabella.
             */
            methodNode.addDecBody(declarations, $body.ast);
            symTable.remove(nestingLevel--);
		      })*                 
          CRPAR 
          {
            /*
             * Il parsing della classe � terminato: posso rimuovere dunque la testa della
             * symbolTable.
             */
            symTable.remove(nestingLevel--);
          } 
          )*
        ; 

declist	returns [ArrayList<Node> astlist]
	      : 
	      {
	        /*
	         * Inizializzo la lista delle dichiarazioni ed imposto l'offset a -2 come da design degli AR.
	         */
	        $astlist= new ArrayList<Node>() ;
	        int offset=-2;
	      }
	      ((VAR i=ID COLON t=type ASS e=exp
        {
          /*
           * Per ogni variabile individuata, creo il VarNode corrispondente
           * e lo aggiungo alla lista delle dichiarazioni. Inoltre, provo ad inserirla
           * nella testa della symbolTable per assicurarmi che non sia una dichiarazione multipla.
           */
          VarNode v = new VarNode($i.text,$t.ast,$e.ast); 
          $astlist.add(v);
          HashMap<String, STentry> hm = symTable.get(nestingLevel); 
          if (hm.put($i.text,new STentry(nestingLevel, $t.ast,offset--)) != null)
          {
            System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
            System.exit(0);
          }
          // Nel caso la variabile sia un ArrowType, questa occupa uno offset doppio.
          if ($t.ast instanceof ArrowTypeNode)
            offset--;
         }  
         | FUN i=ID COLON t=basic
         {  
            /*
             * Nel caso in cui questa sia una funzione, verr� creato un FunNode, aggiunto
             * alla lista delle dichiarazioni, viene eseguito il tentativo di aggiunta nella
             * testa della symbolTable e si scala di due posizioni l'offset dal momento che �
             * a priori un ArrowType. L'aggiunta del FunNode nella tabella corrente fallisce
             * nel momento in cui risulti gi� dichiarato qualcosa con lo stesso nome.
             */
            FunNode f = new FunNode($i.text,$t.ast);
            $astlist.add(f);
            HashMap<String, STentry> hm = symTable.get(nestingLevel);
            STentry entry = new STentry(nestingLevel, offset); 
            offset-=2; //E' un ArrowType
            if (hm.put($i.text,entry) != null)
            {
              System.out.println("Fun id "+$i.text+" at line "+$i.line+" already declared");
              System.exit(0);
            }
            /*
             * Entro in un nuovo scope, dunque aggiorno il nestingLevel ed aggiungo una ulteriore
             * tabella in testa alla symbolTable.
             */
            nestingLevel++;
            HashMap<String, STentry> hmn = new HashMap<String, STentry> ();
            symTable.add(hmn);
         }
         LPAR 
         {
           /*
            * Dal momento che stiamo nello scope interno alla funzione, preparo l'ArrayList per
            * i tipi di parametro in modo da ottenere tutte le informazioni per costruire l'ArrowTypeNode
            * che la rappresenta. Inoltre imposto l'offset dei parametri ad 1, come da design dell'AR.
            */
           ArrayList<Node> parTypes = new ArrayList<Node>();
           int paroffset=1;
         } 
         (fid=ID COLON fty=type
         { 
           /*
            * Catturo uno ad uno i tipi dei parametri, creo il ParNode per ognuno di essi e lo aggiungo
            * al FunNode. Anche qui vale il discorso di avere un offset doppio nel caso in cui il parametro
            * sia un ArrowType. Viene eseguito il controllo sull'aggiunta per verificare che non vi siano
            * dichiarazioni multiple dello stesso parametro.
            */
	         parTypes.add($fty.ast);
	         ParNode fpar = new ParNode($fid.text, $fty.ast);
	         f.addPar(fpar);
	         if ($fty.ast instanceof ArrowTypeNode)
	           paroffset++;
	         if (hmn.put($fid.text, new STentry(nestingLevel, $fty.ast, paroffset++)) != null)
	         {
	           System.out.println("Parameter id "+$fid.text+" at line "+$fid.line+" already declared");
	           System.exit(0);
	         }
	       }
         (COMMA id=ID COLON ty=type
         {
           /*
            * Discorso analogo a quanto detto sopra.
            */
	         parTypes.add($ty.ast); //
	         ParNode par = new ParNode($id.text, $ty.ast);
	         f.addPar(par);
	         if ($ty.ast instanceof ArrowTypeNode)
	           paroffset++;
	         if (hmn.put($id.text, new STentry(nestingLevel, $ty.ast,paroffset++)) != null)
	         {
	           System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
	           System.exit(0);
	         }
	       }
	       )* )? 
	       RPAR 
	       {
	         /*
	          * Con la chiusura della parentesi tonda, ho ottenuto tutti i tipi dei parametri, 
	          * dunque posso decorare la STentry dell'AST rappresentante la funzione
	          *  e definire il tipo del FunNode.
	          */
	         entry.addType(new ArrowTypeNode(parTypes, $t.ast));
	         f.addSymType(new ArrowTypeNode(parTypes, $t.ast));
	         ArrayList<Node> decList = new ArrayList<Node>();
	       }
	       (LET d=declist 
	       {
	         //Ricorsione
	         decList = $d.astlist;
	       } 
	       IN)? e=exp 
	       { 
	         /*
	          * Ho finito di parsare tutto il contenuto della funzione, dunque posso sia rimuovere 
	          * la testa della symbolTable, sia aggiungere al FunNode tutte le dichiarazioni ed il suo corpo.
	          */
	         f.addDecBody(decList,$e.ast);
	         symTable.remove(nestingLevel--);
	       }
         ) SEMIC )+          
	;
	
/*
 * Estensione Higher-order: 
 *  tipi in dichiarazioni (non-terminale "type") ora includono:
 *    tipi di base (non-terminale "basic"): bool e int 
 *    tipi funzionali (non-terminale "arrow"): 
 *      un "basic" come tipo di ritorno e tipi (possibilmente funzionali) "type" per i parametri
 */
type returns [Node ast]
        : b=basic {$ast = $b.ast;} 
        | a=arrow {$ast = $a.ast;} 
        ;

basic returns [Node ast]
        : INT  {$ast = new IntTypeNode();}
        | BOOL {$ast = new BoolTypeNode();}
        /*
         * Estensione Object-oriented
         *  Introduzione di ID come tipo - ClassTypeNode
         */
        | i=ID 
        {
          $ast = new ClassTypeNode($i.text); 
          if (classTable.get($i.text) == null) 
          {
            System.out.println("Class "+$i.text+" at line "+$i.line+" not declared");
            System.exit(0);
          }
        } 
        ;  
    
/*
 * Estensione Higher-order:
 * per i tipi "arrow" creare degli ArrowTypeNode 
 */
arrow returns [Node ast]
        : LPAR 
          {
            ArrayList<Node> params = new ArrayList<Node>();
          }
          (ft=type 
          { 
            params.add($ft.ast); 
          }
          (COMMA t=type
          { 
            params.add($t.ast);
          }
          )* )? 
          /* 
           * La scelta di utilizzare basic al posto di type � legata alla possibilit� di trovare
           * come tipo di un parametro anche un ID che pu� corrispondere ad una call o (successivamente)
           * all'invocazione di un metodo.
           */ 
          RPAR ARROW b=basic
          { 
            $ast = new ArrowTypeNode(params, $b.ast); 
          }
        ;  
	 
exp	returns [Node ast]
 	: f=term {$ast= $f.ast;}
 	    (
 	       PLUS l=term
 	         {$ast= new PlusNode ($ast,$l.ast);}
 	       | MINUS l=term
 	         {$ast= new MinusNode($ast,$l.ast);}
 	       | OR l=term
 	         {$ast= new OrNode($ast,$l.ast);}    
      )* 
 	;
 	
term	returns [Node ast]
	: f=factor {$ast= $f.ast;}
	    (
	      TIMES l=factor
	        {$ast= new MultNode ($ast,$l.ast);}
	      | DIV  l=factor 
          {$ast= new DivNode ($ast,$l.ast);}
        | AND  l=factor
          {$ast= new AndNode ($ast,$l.ast);} 
	    )*
	;
	
factor returns [Node ast]
  : v=value {$ast= $v.ast;}
      (
        EQ l=value 
          {$ast= new EqualNode ($ast,$l.ast);}
        | GE l=value 
          {$ast= new GreaterEqualNode ($ast,$l.ast);}
        | LE l=value 
          {$ast= new LessEqualNode ($ast,$l.ast);}
      )*
    ;    	
 	
value	returns [Node ast]
	: n=INTEGER   
	  {$ast = new IntNode(Integer.parseInt($n.text));}  
	| TRUE 
	  {$ast = new BoolNode(true);}  
	| FALSE
	  {$ast = new BoolNode(false);}  
	| LPAR e=exp RPAR
	  {$ast = $e.ast;}  
	| IF x=exp THEN CLPAR y=exp CRPAR 
		   ELSE CLPAR z=exp CRPAR 
	  {$ast = new IfNode($x.ast,$y.ast,$z.ast);}	 
	| NOT LPAR e=exp RPAR
    {$ast = new NotNode($e.ast);} 
	| PRINT LPAR e=exp RPAR	
	  {$ast = new PrintNode($e.ast);}
	| i=ID 
	  { // Ricerco la dichiarazione nella symbolTable
	    int j = nestingLevel;
	    STentry entry = null; 
	    while (j>=0 && entry == null)
	      entry = (symTable.get(j--)).get($i.text);
	    if (entry == null)
      {
        System.out.println("Id "+$i.text+" at line "+$i.line+" not declared");
        System.exit(0);
      }               
		  $ast = new IdNode($i.text, entry, nestingLevel);
	  }  
	  /*
	   * Estensione Object-oriented: 
	   *   CallNode        ID()
	   *   ClassCallNode   ID.ID() 
	   */
	      (LPAR 
	      {
	        // Questo id � una call, quindi preparo l'array per gli argomenti
	        ArrayList<Node> argList = new ArrayList<Node>();
	      } 
	          (fa=exp 
	          {
	            argList.add($fa.ast);
	          }
	          (COMMA a=exp 
	          {
	            argList.add($a.ast);
	          })* )?	     
	          RPAR 
	          {
	            $ast=new CallNode($i.text,entry,argList,nestingLevel);
	          }  
	      | DOT mi=ID 
	      {
	        // Questo id � un oggetto sul quale sta venendo invocato un metodo, 
	        // quindi preparo l'array per gli argomenti 
	        ArrayList<Node> argList=new ArrayList<>();
	      }
	      LPAR(fa=exp 
			  {
			    argList.add($fa.ast);
			  }
			  (COMMA a=exp   
			  {
			    argList.add($a.ast);
			  }
			  )* ) ? RPAR
			  {
			    if (entry.getType() instanceof ClassTypeNode) 
			    {
            ClassTypeNode type = (ClassTypeNode) entry.getType();
            STentry methodEntry = classTable.get(type.getId()).getVirtualTable().get($mi.text);
            if (methodEntry == null) 
            {
              System.out.println("Method " + $mi.text + " does not exist for type " + type.getId() + " at line " + $mi.line);
              System.exit(0);
            }
            methodEntry.setIsMethod(true);
            $ast = new ClassCallNode($i.text, $mi.text, entry, methodEntry, argList, nestingLevel);
          }
        }   
     )?
     /*
      * Estensione Object-oriented
      *   EmptyNode   null
      *   NewNode     new ID()
      */
  | NULL
    {$ast = new EmptyNode(); }
  | NEW i=ID LPAR 
    {
      CTentry ctEntry = classTable.get($i.text);
      if (ctEntry == null) {
        System.out.println("Class "+$i.text+" at line "+$i.line+" not declared");
        System.exit(0);
      }
      ArrayList<Node> parList = new ArrayList<Node>();
    }
    (fp=exp
    {
      parList.add($fp.ast);
    } 
    (COMMA p=exp
    {
      parList.add($p.ast);
    })* 
    {
      $ast = new NewNode($i.text, parList, ctEntry);
    }
    )? RPAR     
    
     	
 	; 

  	
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/
SEMIC	: ';' ;
COLON	: ':' ;
COMMA	: ',' ;
OR  : '||' ;
AND : '&&' ;
NOT : 'not' ;
EQ	: '==' ;
GE  : '>=' ;
LE  : '<=' ;
ASS	: '=' ;
PLUS	: '+' ;
MINUS : '-' ;
TIMES	: '*' ;
DIV: '/' ;
INTEGER : ('-')?(('1'..'9')('0'..'9')*) | '0';
TRUE	: 'true' ;
FALSE	: 'false' ;
LPAR 	: '(' ;
RPAR	: ')' ;
CLPAR 	: '{' ;
CRPAR	: '}' ;
IF 	: 'if' ;
THEN 	: 'then' ;
ELSE 	: 'else' ;
PRINT	: 'print' ; 
LET	: 'let' ;
IN	: 'in' ;
VAR	: 'var' ;
FUN	: 'fun' ;
INT	: 'int' ;
BOOL	: 'bool' ;
ARROW : '->' ;
CLASS: 'class';
EXTENDS: 'extends';
DOT: '.';
NULL: 'null';
NEW: 'new';

ID 	: ('a'..'z'|'A'..'Z')
 	  ('a'..'z'|'A'..'Z'|'0'..'9')* ;

WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    { $channel=HIDDEN; } ;

COMMENT : '/*' .* '*/' { $channel=HIDDEN; } ;
 
ERR   	 : . { System.out.println("Invalid char: "+$text); lexicalErrors++; $channel=HIDDEN; } ; 

