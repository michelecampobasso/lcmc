package ast;

import lib.FOOLlib;

public class AndNode implements Node {

	private Node left;
	private Node right;

	public AndNode(Node l, Node r) {
		left = l;
		right = r;
	}

	public String toPrint(String s) {
		return s + "And:\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	public Node typeCheck() {
		if (!(FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode())
				&& FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode()))) {
			System.out.println("Non boolean in expression");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		String l3 = FOOLlib.freshLabel();
		return left.codeGeneration()+
			"push 1\n"+ // Controllo che il primo sia vero
			"beq "+ l1 +"\n"+ // Se vero, vado su l1 e vedo se il secondo � vero
			"b " + l3 + "\n" + // Fallito, non saranno mai entrambi veri
			l1 + ":\n"+ // Controllo che il secondo sia vero
			right.codeGeneration()+
			"push 1\n" + 
			"beq "+ l2 +"\n"+
			"b " + l3 + "\n"+ // Right non � vero, pusho 0
			l2 + ":\n"+
			"push 1\n"+ // Entrambi veri, pusho 1
			l3 + ":\n" +
			"push 0\n"; // Proseguo dal fallimento
	}

}