package ast;

import lib.FOOLlib;

public class NotNode implements Node {

	private Node expression;

	public NotNode(Node e) {
		expression = e;
	}

	public String toPrint(String s) {
		return s + "Not\n" + expression.toPrint(s + "  ");
	}

	public Node typeCheck() {
		Node e = expression.typeCheck();
		if (!(FOOLlib.isSubtype(e, new BoolTypeNode()))) {
			System.out.println("Incompatible types in negation");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		return expression.codeGeneration() + 
			"push 1\n" +
			"beq " + l1 + "\n" + // Confronto con 1: se non sono uguali vuol dire che l'espressione ha generato 0, quindi pusho 1
			"push 1\n" + 
			"b " + l2 + "\n" +
			l1 + ":\n" + 
			"push 0\n" + // Se la beq � andata, vuol dire che l'espressione ha generato 1, quindi pusho 0 
			l2 + ":\n";
	}
}