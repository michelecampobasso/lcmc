package ast;

public class IdNode implements Node {

	private String id;
	private STentry entry;
	private int nestinglevel;

	public IdNode(String i, STentry st, int nl) {
		id = i;
		entry = st;
		nestinglevel = nl;
	}

	public String toPrint(String s) {
		return s + "Id: " + id + " at nestlev " + nestinglevel + "\n" + entry.toPrint(s + "  ");
	}

	public Node typeCheck() {
		/*
		 * Estensione Higher-order:
		 * ora ammettere anche un ID con tipo funzionale!
		 */
		
		/*
		 * if (entry.getType() instanceof ArrowTypeNode) { //
		 * System.out.println("Wrong usage of function identifier");
		 * System.exit(0); }
		 */
		
		/*
		 * Estensione Object-oriented:
		 * 	controlla che non sia un metodo o il nome di una classe (tipo "null")
		 */
		
		if (entry.getIsMethod()) {
			System.out.println("Id " + id + " is a method and cannot be compared.");
			System.exit(0);
		}
		
		if (entry.getType() == null) {
			System.out.println("Id " + id + " is a class and cannot be compared.");
			System.exit(0);
		}
		
		return entry.getType();
	}

	public String codeGeneration() {
		String getAR = "";
		for (int i = 0; i < nestinglevel - entry.getNestinglevel(); i++)
			getAR += "lw\n";
		
		/*
		 * Estensione Higher-order:
		 * 	ora ammettere anche un ID con tipo funzionale!
		 * 	se il tipo � funzionale (perch� nome di funzione o var/par di tipo funzionale) 
		 * 	due cose sono messe nello stack recuperandole come valori dall'AR dove �
		 * 	dichiarato l'ID con meccanismo usuale di risalita catena statica:
		 * 		1. FP ad AR dichiarazione funzione
		 *		2. (a offset-1) indir funzione
		 * 
		 */
		if (entry.getType() instanceof ArrowTypeNode) {
			return  "lfp\n" + getAR + // risalgo la catena statica
					"push " + entry.getOffset() + "\n" + // calcola indirizzo offset+$fp
					"add\n" + "lw\n"+ // carica sullo stack il valore a quell'indirizzo

					"lfp\n" + getAR +
					"push " + entry.getOffset() + "\n" + //prendo anche il return address che si trova all'indirizzo -1
					"push 1\n" +
					"sub\n" +
					"add\n" + "lw\n";
		}
		return "push " + entry.getOffset() + "\n" + // calcola indirizzo offset+$fp
				"lfp\n" + getAR + // risalgo la catena statica
				"add\n" + "lw\n"; // carica sullo stack il valore a
									// quell'indirizzo

	}
}