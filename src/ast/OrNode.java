package ast;

import lib.FOOLlib;

public class OrNode implements Node {

	private Node left;
	private Node right;

	public OrNode(Node l, Node r) {
		left = l;
		right = r;
	}

	public String toPrint(String s) {
		return s + "Or\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	public Node typeCheck() {
		if (!(FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode())
				&& FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode()))) {
			System.out.println("Non boolean in expression");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		return left.codeGeneration()+
			"push 1\n"+ // Provo il primo: faccio il confronto tra la condizione left ed uno
			"beq "+ l1 +"\n"+ // Se vero, vado su l1 e sono gi� apposto;
			right.codeGeneration()+ //Se sono qui, vuol dire che il primo branch non � andato a buon fine; riprovo con right
			"push 1\n"+
			"beq "+ l1 +"\n"+ // Ritento il controllo per right
			"push 0\n"+ // Fallito anche questa volta, pusho sullo stack 0
			"b " + l2 + "\n" + //Proseguo (ho gi� pushato 0) 
			l1 + ":\n"+ //Andata a buon fine
			"push 1\n"+ 
			l2 + ":\n"; 
	}

}