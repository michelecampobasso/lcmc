package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class MethodNode implements Node, DecNode {

	private String id;
	private Node type;
	private ArrayList<Node> parlist = new ArrayList<Node>();
	private ArrayList<Node> declist = new ArrayList<Node>();
	private Node body;
	private Node symType;
	private String label;
	
	public MethodNode(String i, Node t) {
		id = i;
		type = t;
		symType = new ArrowTypeNode(new ArrayList<Node>(), type);
	}
	
	public void addPar(Node par) {
		parlist.add(par);
	}
	
	public void addSymType(Node ret) {
		symType = ret;
	}
	
	public void addDecBody(ArrayList<Node> decs, Node b) {
		declist = decs;
		body = b;
	}
	
	public String getId() {
		return id;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String l) {
		label = l;
	}
	
	@Override
	public Node getSymType() {
		return symType;
	}
	
	@Override
	public String toPrint(String indent) {
		String parStr = "";
		for (Node par : parlist)
			parStr += indent + par.toPrint(indent + "  ");
		String decStr = "";
		for (Node dec : declist) 
			decStr += indent + dec.toPrint(indent);
		return indent + "Method " + id + ": " + ((ArrowTypeNode)symType).getRet().toPrint("") + (parStr.isEmpty() ? "" : indent + indent +  "Parameters:\n" + parStr) + (decStr.isEmpty() ? "" : indent + indent + "Declarations:\n" + decStr);
	}

	@Override
	public Node typeCheck() {
		if (parlist!=null) 
			for (Node dec:parlist)
				dec.typeCheck();
		if ( !(FOOLlib.isSubtype(body.typeCheck(),type)) ){
			System.out.println("Wrong return type for function "+id);
			System.exit(0);
		}  
		return null;
	}

	@Override
	public String codeGeneration() {
		String deccode = "";
		String funl = label;
		String popdec = "";
		DecNode p;
		if (declist != null) {

			for (int i = 0; i < declist.size(); i++)
				deccode = deccode + (declist.get(i)).codeGeneration();

			for (int i = 0; i < declist.size(); i++) {
				p = (DecNode) declist.get(i);
				if (p.getSymType() instanceof ArrowTypeNode)
					popdec += "pop\n";
				popdec += "pop\n";

			}
		}
		String poppar = "";
		for (int i = 0; i < parlist.size(); i++) {
			p = (DecNode) parlist.get(i);
			if (p.getSymType() instanceof ArrowTypeNode)
				poppar += "pop\n";
			poppar += "pop\n";

		}
		
		FOOLlib.putCode(
				funl + ":\n" +
				"cfp\n" + // aggiorno il valore del frame pointer con quello dello stack pointer
				"lra\n" + // carico sullo stack il l'indirizzo al quale dovr� tornare dopo l'esecuzione del metodo
				deccode + 
				body.codeGeneration() + // operazioni del metodo 
				"srv\n" + // salvo il valore di ritorno del metodo nel registro rv
				popdec + // rimuovo le dichiarazioni
				"sra\n" + // salvo il l'indirizzo a cui tornare nel registro ra
				"pop\n" + // rimuovo l'AL
				poppar + // rimuovo i parametri
				"sfp\n" + // aggiorno il frame pointer
				"lrv\n" + // metto sullo stack valore di ritorno...
				"lra\n" + // ...e indirizzo a cui tornare...
				"js\n" // e ci salto
		);

		return "";
	}
}
