package ast;

public class FieldNode implements Node, DecNode {

	private String id;
	private Node type;

	public FieldNode(String i, Node t) {
		id = i;
		type = t;
	}

	public String getId() {
		return id;
	}

	@Override
	public Node getSymType() {
		return type;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Field " + id + ":" + type.toPrint(" ");
	}

	@Override
	public Node typeCheck() {
		return type;
	}

	@Override
	public String codeGeneration() {
		return type.codeGeneration();
	}
}
