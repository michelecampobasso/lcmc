package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class ClassCallNode implements Node {
	
	private String id;
	private String methodId;
	private STentry entry;
	private STentry methodEntry;
	private ArrayList<Node> argList = new ArrayList<Node>();
	private int nestingLevel;

	public ClassCallNode(String i, String mi, STentry e, STentry me, ArrayList<Node> al, int nl) {
		id = i;
		methodId = mi;
		entry = e;
		methodEntry = me;
		argList = al;
		nestingLevel = nl;
	}
	
	@Override
	public String toPrint(String s) {
		String str = s+ s + "Class call " + id + " on method " + methodId;
		if (argList.size()==0)
			str += " without params.\n" + s;
		else {
			str +=  " \n" + s + s + "  Parameter types: \n";
			for (int j = 0; j < argList.size(); j++)
				str += s + "  " + argList.get(j).toPrint(s + "  ");
			str += "\n" + s;
		}
		return str;
	}

	@Override
	public Node typeCheck() {
		if (!(methodEntry.getType() instanceof ArrowTypeNode)) {
			System.out.println("Invocation of a non-method " + id);
			System.exit(0);
		}
		
		ArrayList<Node> args = ((ArrowTypeNode)methodEntry.getType()).getParList();
		if (!(args.size() == argList.size())) {
			System.out.println("Wrong number of arguments in the invocation of " + id);
			System.exit(0);
		}
		
		for (int i = 0; i < argList.size(); i++)
			if (!(FOOLlib.isSubtype((argList.get(i)).typeCheck(), args.get(i)))) {
				System.out.println("Wrong type for " + (i + 1) + "-th argument in the invocation of " + id);
				System.exit(0);
			}
		return methodEntry.getType().typeCheck();
	}

	@Override
	public String codeGeneration() {
		String parCode = "";	
		for(int i=argList.size()-1; i>=0; i--){
			parCode += argList.get(i).codeGeneration();
		}
		
		String getAR = "lfp\n";
		for(int i=0; i< nestingLevel - entry.getNestinglevel(); i++)
			// Differenza livello in cui sono - livello in cui � dichiarata la funzione -> chiamante-chiamato
			getAR += "lw\n";
		
		// Recupera valore dell'ID1 (object pointer) dall'AR dove � dichiarato con meccanismo usuale di risalita catena statica		
		return 
				"lfp\n" + 
				parCode +	
				getAR + // AR dove � dichiarato l'oggetto
				"push " + entry.getOffset()+"\n"+ // Offset della dichiarazione dell'oggetto
				"add\n" + // Calcolo la sua posizione 
				"lw\n" + // Carico il riferimento all'oggetto sullo stack
				getAR +  // Ricalcolo nuovamente l'AR dov'� stato dichiarato l'oggetto
				"push " + entry.getOffset() + "\n"+
				"add\n" +
				"lw\n" +	 // Risolvo il riferimento all'oggetto
				"push " + methodEntry.getOffset() + "\n" + // Calcolo su di esso la posizione del riferimento al metodo...  		
				"add\n" +
				"lw\n" + // ...e la risolvo
				"js\n"; // In questo modo, salto all'indirizzo del metodo, eseguo e salvo come ReturnAddress il riferimento all'oggetto
		}
}
