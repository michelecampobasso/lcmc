package ast;

public class EmptyTypeNode implements Node {

	@Override
	public String toPrint(String indent) {
		return indent;
	}

	@Override
	// Never used
	public Node typeCheck() {
		return null;
	}

	@Override
	// Never used
	public String codeGeneration() {
		return "";
	}

}
