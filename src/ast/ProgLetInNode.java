package ast;

import java.util.ArrayList;
import lib.*;

public class ProgLetInNode implements Node {

	private ArrayList<Node> cllist;
	private ArrayList<Node> declist;
	private Node exp;

	public ProgLetInNode(ArrayList<Node> c, ArrayList<Node> d, Node e) {
		cllist = c;
		declist = d;
		exp = e;
	}

	public String toPrint(String s) {
		String clstr = "";
		for (Node cl: cllist)
			clstr += cl.toPrint(s + "  ");
		String declstr = "  ";
		for (Node dec : declist)
			declstr += dec.toPrint("");
		return s + "ProgLetIn\n" + clstr + declstr + "\n" + s + "Expression:\n" + exp.toPrint(s + "  ");
	}

	public Node typeCheck() {
		for (Node cl : cllist)
			cl.typeCheck();
		for (Node dec : declist)
			dec.typeCheck();
		return exp.typeCheck();
	}

	public String codeGeneration() {
		String clCode = "";
		for (Node cl : cllist)
			clCode += cl.codeGeneration();
		String declCode = "";
		for (Node dec : declist)
			declCode += dec.codeGeneration();
		return "push 0\n" + clCode + declCode + exp.codeGeneration() + "halt\n" + FOOLlib.getCode();
	}
}