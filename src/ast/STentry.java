package ast;

public class STentry {

	private int nl;
	private Node type;
	private int offset;
	// Estensione Object-oriented: i metodi vanno trattati diversamente
	private boolean isMethod = false;

	// Costruttore per oggetti (type == null)
	public STentry(int n, int os) {
		nl = n;
		offset = os;
	}
	
	public STentry(int n, Node t, int os) {
		nl = n;
		type = t;
		offset = os;
	}

	public void addType(Node t) {
		type = t;
	}

	public Node getType() {
		return type;
	}

	public int getOffset() {
		return offset;
	}

	public int getNestinglevel() {
		return nl;
	}
	
	public void setIsMethod(boolean is) {
		isMethod = is;
	}
	
	public boolean getIsMethod() {
		return isMethod;
	}

	public String toPrint(String s) {
		return (isMethod ? s + "STentry: method\n" : "") + s + "STentry: nestlev " + Integer.toString(nl) + "\n" + s + "STentry: type\n" + (type != null ? type.toPrint(s + "  ") : "")
				+ s + "STentry: offset " + Integer.toString(offset) + "\n";
	}
}