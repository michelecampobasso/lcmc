package ast;

import java.util.ArrayList;
import java.util.HashSet;

import lib.FOOLlib;

public class ClassNode implements Node {

	private String id;
	private CTentry classEntry;
	private CTentry superEntry;
	private ArrayList<Node> fieldList = new ArrayList<Node>();
	private ArrayList<Node> methodList = new ArrayList<Node>();
	
	
	public ClassNode(String i, CTentry e, CTentry st) {
		id = i;
		classEntry = e;
		superEntry = st;
	}
	
	public void addField(Node field) {
		fieldList.add(field);
	}
	
	public void addMethod(Node method) {
		methodList.add(method);
	}

	public String toPrint(String s) {
		String methods = "";
		String fields = "";
		for (Node m : methodList)
			methods += s + m.toPrint(s + s);
		for (Node f : fieldList)
			fields += s + f.toPrint(s + s);
		return s + "Class: " + id + ", SuperType: " + (superEntry == null ? "None" : superEntry.getId()) + "\n" + 
					classEntry.toPrint(s + "  ") + "\n" + s +
					"Declared in the class: \n" + s + s +
					"Methods: \n" + methods + s + s +
					"Fields: \n" + fields + "\n";
	}
	
	public Node typeCheck() {
	
		/* 
		 * La classe non ha un supertipo, dunque posso procedere tranquillamente a lanciare
		 * il typeCheck su ogni campo e metodo presente in essa:
		 */
		if (superEntry==null) {
			for (Node meth : classEntry.getAllMethods())
				meth.typeCheck();
			for (Node field : classEntry.getAllFields())
				field.typeCheck();
		}
		
		/*
		 *  La classe � ereditata, dunque controllo le dichiarazioni locali, overridden e non 
		 */
		else {
			/*
			 * in caso di ereditariet� controlla che l'overriding sia di campi che di metodi 
			 * sia corretto usando i campi allFields e allMethods delle CTentry in classEntry e superEntry:
			 * in particolare usa l'HashSet "locals" della CTentry in classEntry per stabilire su quali 
			 * campi e metodi sia veramente necessario fare tale controllo
			 */
			HashSet<Integer> locals = classEntry.getLocals();

			for (int offset : locals) {
				// Campo
				if (offset<0) {
					// Offset invertito di segno e diminuito di 1 serve per ottenere il campo corrispondente dall'ArrayList 
					FieldNode local = ((FieldNode)classEntry.getAllFields().get((offset*-1)-1));
					FieldNode overridden;
					// Overridden field?
					try {
						if ((overridden = ((FieldNode)superEntry.getAllFields().get((offset*-1)-1)))!=null) 
							if (!FOOLlib.isSubtype(((FieldNode)local).getSymType(), ((FieldNode)overridden).getSymType())) {
								System.out.println("Field " + local.getId() + " has an invalid type for the extended class.");
								System.exit(0);
							}
					} catch (IndexOutOfBoundsException e) {
						/*
						 * Invece di tornare null, quando si accede ad una posizione nulla dell'ArrayList, 
						 * viene sollevata eccezione IndexOutOfBoundsException. Il significato per noi � che
						 * non � un campo dichiarato nella superclasse e dunque non va controllato il subTyping
						 * tra l'eventuale dichiarato ed overridden.
						 */
					}
					// Lancio il typeCheck su
					local.typeCheck();
				}
				// Metodo
				else {
					MethodNode local = ((MethodNode)classEntry.getAllMethods().get(offset));
					MethodNode overridden;
					// Overridden method?
					try {
						if ((overridden = ((MethodNode)superEntry.getAllMethods().get(offset)))!=null)
							// La controvarianza viene controllata dal metodo di libreria isSubtype
							if (!FOOLlib.isSubtype(((MethodNode)local).getSymType(), ((MethodNode)overridden).getSymType())) {
								System.out.println("Method " + local.getId() + " has an invalid type for the extended class.");
								System.exit(0);
							}
					} catch (IndexOutOfBoundsException e) {
						// Do nothing, just skip that
					}
					local.typeCheck();
				}
			}
		}
		// Creo il tipo
		return new ClassTypeNode(id);
	}

	public String codeGeneration() {
		// Generazione delle label
		for (Node meth : classEntry.getAllMethods())
			// Non eredita da nessuno, tutti i metodi devono essere etichettati:
			if (superEntry == null)
				((MethodNode)meth).setLabel(FOOLlib.freshFunLabel());
			else 
				// Diversamente, se la classe � ereditata, assegno una nuova label solo per quei metodi che non l'hanno ancora ottenuta
				if (!superEntry.getAllMethods().contains(((MethodNode)meth).getId()))
					((MethodNode)meth).setLabel(FOOLlib.freshFunLabel());
		// Generazione codice sui metodi ornati di label
		for (Node meth : classEntry.getAllMethods())
			meth.codeGeneration();
		return "";
	}
}