package ast;

public class ClassTypeNode implements Node {

	String id;
	
	public ClassTypeNode(String i) {
		id = i;
	}
	
	public String getId() {
		return id;
	} 
	
	@Override
	public String toPrint(String indent) {
		return indent + id + "\n";
	}

	@Override
	public Node typeCheck() {
		return null;
	}

	@Override
	public String codeGeneration() {
		return "";
	}

}
