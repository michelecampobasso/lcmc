package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class CallNode implements Node {

	private String id;
	private STentry entry;
	private ArrayList<Node> parlist;
	private int nestinglevel;

	public CallNode(String i, STentry e, ArrayList<Node> p, int nl) {
		id = i;
		entry = e;
		parlist = p;
		nestinglevel = nl;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist)
			parlstr += par.toPrint(s + "  ");
		return s + "Call:" + id + " at nestlev " + nestinglevel + "\n" + entry.toPrint(s + "  ") + parlstr;
	}

	public Node typeCheck() {
		ArrowTypeNode t = null;
		if (entry.getType() instanceof ArrowTypeNode)
			t = (ArrowTypeNode) entry.getType();
		else {
			System.out.println("Invocation of a non-function " + id);
			System.exit(0);
		}
		ArrayList<Node> p = t.getParList();
		if (!(p.size() == parlist.size())) {
			System.out.println("Wrong number of parameters in the invocation of " + id);
			System.exit(0);
		}
		for (int i = 0; i < parlist.size(); i++)
			if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(), p.get(i)))) {
				System.out.println("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
				System.exit(0);
			}
		return t.getRet();
	}

	public String codeGeneration() {

		String parCode = "";
		for (int i = parlist.size() - 1; i >= 0; i--)
			parCode += (parlist.get(i)).codeGeneration();

		String lookupAR = "lfp\n";
		for (int i = 0; i < nestinglevel - entry.getNestinglevel(); i++)
			lookupAR += "lw\n";

		/* 
		 * Estensione Higher-order:
		 * il tipo deve essere funzionale (perch� nome di funzione o var/par di tipo funzionale)
		 * due cose sono recuperate come valori dall'AR dove � dichiarato l'ID 
		 * con meccanismo usuale di risalita catena statica:
		 * - FP ad AR dichiarazione funzione (usato per settare nuovo Access Link AL)
		 * - (a offset-1) indir di funzione (usato per saltare a codice funzione)
		 */
		return 	"lfp\n"+ 
	    		parCode+  
	    		lookupAR+ // AL corrente + tante lw quanti sono i nl da risalire
	    		"push "+entry.getOffset()+"\n"+  
	    		"add\n"+ // Calcolo l'indirizzo con l'offset corretto...
	    	    "lw\n"+ // ...e carico il valore sullo stack
	    	    lookupAR+ // nuovamente individuo l'AL dell'AR in cui � stato dichiarato entry
	    	    "push "+entry.getOffset()+"\n"+
	    	    "push 1\n"+ //...e calcolo l'offset decrementato di 1 per ottenere anche il return address...
	    	    "sub\n"+
	    	    "add\n"+
	    	    "lw\n"+ // ...e lo porto sulla cima dello stack.
	    	    "js\n"; // Salto all'indirizzo della call e mantengo il return address nell'apposito registro
	}

}