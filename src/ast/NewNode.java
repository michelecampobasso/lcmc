package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class NewNode implements Node {

	String id;
	ArrayList<Node> parList = new ArrayList<Node>();
	CTentry entry;
	
	public NewNode(String i, ArrayList<Node> pl, CTentry e) {
		id = i;
		parList = pl;
		entry = e;
	}
	
	@Override
	public String toPrint(String indent) {
		return indent;
	}

	@Override
	public Node typeCheck() {
		if (parList != null) {
			if (parList.size() != entry.getAllFields().size()) {
				System.out.println("Wrong number of parameters for constructor " + id );
				System.exit(0);
			}
			for (int i = 0; i < parList.size(); i++) {
				if (!(FOOLlib.isSubtype((parList.get(i)).typeCheck(), entry.getAllFields().get(i).typeCheck()))) {
					System.out.println("Wrong type for " + (i+1) + "-th parameter in the constructor of " + id);
					System.exit(0);
				}
			}
		} else {
			if (entry.getAllFields() != null) {
				System.out.println("Wrong number of parameters for the constructor of " + id);
				System.exit(0);
			}
		}
		return new ClassTypeNode(id);
	}

	@Override
	public String codeGeneration() {
		
		/* 
		 * caso particolare: per classe ID senza metodi (nemmeno ereditati) allocare per i metodi 
		 * uno spazio fittizio di grandezza 1 a cui punta l'object pointer messo sullo stack
		 * (se per i metodi non si alloca nulla due oggetti creati possono essere == tra loro)
		 */
		
		if (entry.getVirtualTable().isEmpty()) // Si suppone che una classe con soli campi non esista
			return "lhp\n" + // Carico sullo stack il puntatore alla posizione dell'oggetto vuoto che sto per creare
				"lhp\n" + // Lo carico di nuovo e ne calcolo la posizione successiva per poter lasciare uno spazio vuoto
				"push 1\n" +
				"add\n" +
				"shp\n"; // Salvo l'heap pointer aggiornato (ho creato un oggetto che occupa una singola posizione in memoria)
				
		else {
			
			String methodCode = "";
			String fieldCode = "";
			
			for (int i=parList.size()-1; i>=0; i--)
				/*
				 * Inserisco hp sullo stack, tramite sw inserisco l'hp nel registro address, faccio una seconda pop
				 * e metto in memoria all'indirizzo ottenuto con la precedente pop il valore presente sulla cima dello stack (il parametro).
				 * Dopodich� ricalcolo l'hp con la posizione successiva e lo aggiorno.
				 * Notare che l'inversione dei parametri la ottengo scorrendo la lista dalla fine all'inizio.
				 */
				fieldCode += parList.get(i).codeGeneration() +
					"lhp\n" +  
					"sw\n"+ 
					"lhp\n"+
					"push 1\n"+ 
					"add\n"+ 
					"shp\n" ;

			for (int i=0; i<entry.getAllMethods().size(); i++)
				/*
				 * Identico a sopra.
				 */
				methodCode += "push " + ((MethodNode)entry.getAllMethods().get(i)).getLabel()+"\n"+
						"lhp\n" +
						"sw\n"+  // Carico in memoria all'indirizzo dell'heap pointer la label del metodo
						"lhp\n"+
						"push 1\n"+
						"add\n"+ // Ricalcolo HP
						"shp\n";  // Aggiorno HP
			
			return fieldCode + "lhp\n" + methodCode;
			
		}
	}
}