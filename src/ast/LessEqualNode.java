package ast;

import lib.FOOLlib;

public class LessEqualNode implements Node {

	private Node left;
	private Node right;

	public LessEqualNode(Node l, Node r) {
		left = l;
		right = r;
	}

	public String toPrint(String s) {
		return s + "Less Equal:\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	public Node typeCheck() {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l))) {
			System.out.println("Incompatible types in less equal");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		return left.codeGeneration() + 
				right.codeGeneration() + 
				"bleq " + l1 + "\n" + // Branch less equal: left <= right ?
				"push 0\n" + // left > right
				"b " + l2 + "\n" + 
				l1 + ":\n" + 
				"push 1\n" + // left <= right
				l2 + ":\n"; // left > right
	}
}