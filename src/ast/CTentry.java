package ast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class CTentry implements Cloneable {
	
	private String id;
	private HashMap<String, STentry> vTable = new HashMap<String, STentry>();
	private int offsetFields = -1;
	private int offsetMethods = 0;
	private ArrayList<Node> allFields = new ArrayList<Node>();
	// Questa � la dispatch table: contiene i riferimenti a tutti i metodi visibili alla classe
	private ArrayList<Node> allMethods = new ArrayList<Node>();
	// Questo HashSet serve a tenere traccia di ci� che viene definito all'interno di una classe
	// per evitare di incorrere in ridefinizioni di campi e metodi:
	private HashSet<Integer> locals = new HashSet<Integer>();
	
	public CTentry(String i) {
		id = i;
	}
	
	public HashMap<String, STentry> getVirtualTable() {
		return vTable;
	}
	
	private boolean isLocalRedefinition(Node toCheck) {
		
		/*
		 * Tale metodo si occupa di controllare se il metodo o il campo sono stati ridefiniti localmente. Per ottenere questo risultato, viene 
		 * cercata la STentry del nodo in questione all'interno della virtual table. Da qui, ottengo l'offset della dichiarazione e provo a cercarlo
		 * all'interno di locals. Se � presente tale offset, allora vuol dire che ci si trova davanti ad una ridefinizione e va segnalato l'errore.  
		 */
		String toCheckId;
		
		if (toCheck instanceof MethodNode) 
			toCheckId = ((MethodNode) toCheck).getId();
		else
			toCheckId = ((FieldNode) toCheck).getId();
		
		STentry toCheckSTentry = vTable.get(toCheckId);
		if (toCheckSTentry == null) // Non � mai stato definito
			return false;
		
		int offset = toCheckSTentry.getOffset();
		for (Integer off : locals)
			if (offset == off)
				return true;
		
		return false;
	}
	
	public STentry addField(Node field) {
		
		if (isLocalRedefinition(field)) {
			System.out.println("Redefinition of field " + ((FieldNode)field).getId() + " in class " + id + "\n");
			System.exit(0);
		}
		
		FieldNode f;
		
		int overriddenOffset = 0;
		int shift = -1;
		for (int i = 0; i < allFields.size(); i++) {
			f = (FieldNode) allFields.get(i);
			// Override...
			if(f.getId().equals(((FieldNode)field).getId()))
			{
				overriddenOffset = vTable.get(f.getId()).getOffset();
				allFields.remove(i);
				shift = i;
				offsetFields++;
			}
		}
		// Il nestingLevel da specifiche sar� sempre 1
		STentry stEntry = new STentry(1, ((FieldNode)field).getSymType(), overriddenOffset == 0 ? offsetFields : overriddenOffset);
		vTable.put(((FieldNode)field).getId(), stEntry);
		
		addFieldInAllFields(field, shift);
		
		locals.add(overriddenOffset == 0 ? offsetFields : overriddenOffset);
		offsetFields--;
		return stEntry;
	}

	public STentry addMethod(Node method) {
		
		if (isLocalRedefinition(method)) {
			System.out.println("Redefinition of method " + ((MethodNode)method).getId() + " in class " + id + "\n");
			System.exit(0);
		}
		
		MethodNode m;
		
		/* 
		 * Dal momento che ogni STentry ha un suo specifico offset, non � sufficiente in caso di override rimuovere la entry,
		 * decrementare il contatore e settare l'offset della entry overridden con quello nuovo: se ci trovassimo nel caso in cui,
		 * in ordine di apparizione, all'interno di una classe ci siano prima nuove entry e poi quelle overridden, ci troveremmo 
		 * davanti a casi di inconsistenza nel layout, dove le nuove entry occupano posizioni in cui andrebbero quelle overridden.
		 * Si tiene dunque traccia dell'offset della entry e, in caso di override, la entry aggiornata occuper� il posto di quella
		 * ereditata. 
		 */
		int overriddenOffset = -1;
		int shift = -1;
		for (int i = 0; i < allMethods.size(); i++){
			m = (MethodNode) allMethods.get(i);
			// Override...
			if(m.getId().equals(((MethodNode)method).getId())) {
				overriddenOffset = vTable.get(m.getId()).getOffset();
				allMethods.remove(i);
				// Spiegazione di shift sull'invocazione del metodo addFieldsInAllFields(...)
				shift = i;
				offsetMethods--;
			}
		}
		// Il nestingLevel da specifiche sar� sempre 1
		STentry stEntry = new STentry(1, ((MethodNode)method).getSymType(), overriddenOffset < 0 ? offsetMethods : overriddenOffset);
		stEntry.setIsMethod(true);
		vTable.put(((MethodNode)method).getId(), stEntry);
		
		/*
		 * Nel momento in cui c'� stato l'override, se il campo si trova in una posizione intermedia all'interno
		 * dell'ArrayList allFields, questo quando viene rimosso fa shiftare gli altri elementi presenti e quindi
		 * quando viene aggiunto nuovamente, questo si trova in una posizione errata ed in fase di code generation
		 * produce un layout non coerente e si ha errore. In questo modo, � possibile prevenire tale caso.
		 */
		addMethodInAllMethods(method, shift);
		
		locals.add(overriddenOffset < 0 ? offsetMethods : overriddenOffset);
		offsetMethods++;
		return stEntry;
	}
	
	private void addFieldInAllFields(Node field, int shift) {
		if(shift > -1) {
			ArrayList<Node> shifter = new ArrayList<Node>();
			int j = allFields.size(); // Non posso usarla direttamente nel for perch� questa cambia!
			for (int i=0; i<j-shift; i++)
				shifter.add(allFields.remove(shift));
			allFields.add(field);
			j = shifter.size();
			for (int i=0; i<j; i++)
				allFields.add(shifter.remove(0));
		} else
			allFields.add(field);
	}
	
	private void addMethodInAllMethods(Node method, int shift) {
		if(shift > -1) {
			ArrayList<Node> shifter = new ArrayList<Node>();
			int j = allMethods.size(); // Non posso usarla direttamente nel for perch� questa cambia!
			for (int i=0; i<j-shift; i++)
				shifter.add(allMethods.remove(shift));
			allMethods.add(method);
			j = shifter.size();
			for (int i=0; i<j; i++)
				allMethods.add(shifter.remove(0));
		} else
			allMethods.add(method);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CTentry clone() {
		
		CTentry clown = new CTentry(this.id);
		clown.allFields = (ArrayList<Node>) this.allFields.clone();
		clown.allMethods = (ArrayList<Node>) this.allMethods.clone();
		clown.offsetFields = this.offsetFields;
		clown.offsetMethods = this.offsetMethods;
		clown.vTable = (HashMap<String, STentry>) this.vTable.clone();
		clown.locals = new HashSet<Integer>();
		return clown;
	}
	
	public String getId() {
		return (id == null ? "None" : id);
	}
	
	public void setId(String i) {
		id = i;
	}
	
	public ArrayList<Node> getAllFields() {
		return allFields;
	}
	
	public ArrayList<Node> getAllMethods() {
		return allMethods;
	}
	
	protected HashSet<Integer> getLocals() {
		return locals;
	}

	public String toPrint(String s) {
		String vtableString = "";
		Set<String> vtableKeys = vTable.keySet();
		for (String key : vtableKeys)
			vtableString += s + " " + key + ":\n" + vTable.get(key).toPrint(s + "   ");
		return s + "CTentry: offsetFields " + Integer.toString(offsetFields) + "\n" + s + "CTentry: offsetMethods "
				+ Integer.toString(offsetMethods) + "\n" + s + "CTentry: vTable\n" + vtableString;
	}
}
