package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class FunNode implements Node, DecNode {

	private String id;
	private Node type;
	private ArrayList<Node> parlist = new ArrayList<Node>();
	private ArrayList<Node> declist = new ArrayList<Node>();
	private Node body;
	private ArrowTypeNode symType;

	public FunNode(String i, Node t) {
		id = i;
		type = t;
		symType = new ArrowTypeNode(new ArrayList<Node>(), type);
	}

	public void addDecBody(ArrayList<Node> d, Node b) {
		declist = d;
		body = b;
	}

	public void addPar(Node p) {
		parlist.add(p);
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist)
			parlstr += par.toPrint(s + "  ");
		String declstr = "";
		if (declist != null)
			for (Node dec : declist)
				declstr += dec.toPrint(s + "  ");
		return s + "Fun:" + id + "\n" + type.toPrint(s + "  ") + parlstr + declstr + body.toPrint(s + "  ");
	}

	// valore di ritorno non utilizzato
	public Node typeCheck() {
		if (declist != null)
			for (Node dec : declist)
				dec.typeCheck();
		if (!(FOOLlib.isSubtype(body.typeCheck(), type))) {
			System.out.println("Wrong return type for function " + id);
			System.exit(0);
		}
		return null;
	}

	/*
	 * Estensione Higher-order:
	 * 	due cose sono messe nello stack:
	 * 		1. FP a questo AR (in reg $fp)
	 * 		2. (a offset-1) indir di f
	 * 
	 * 	in caso tra i parametri o le dichiarazioni vi siano ID di tipo 
	 * 	funzionale (usare getSymType() su DecNode) si devono de-allocare
	 * 	due cose dallo stack (con due "pop")
	 */
	public String codeGeneration() {
		String deccode = "";
		String funl = FOOLlib.freshFunLabel();
		String popdec = "";
		DecNode p;
		if (declist != null) {

			for (int i = 0; i < declist.size(); i++)
				deccode = deccode + (declist.get(i)).codeGeneration();

			for (int i = 0; i < declist.size(); i++) {
				p = (DecNode) declist.get(i);
				if (p.getSymType() instanceof ArrowTypeNode)
					popdec += "pop\n";
				popdec += "pop\n";

			}
		}
		String poppar = "";
		for (int i = 0; i < parlist.size(); i++) {
			p = (DecNode) parlist.get(i);
			if (p.getSymType() instanceof ArrowTypeNode)
				poppar += "pop\n";
			poppar += "pop\n";

		}

		FOOLlib.putCode(
				funl + ":\n" +
				"cfp\n" + // Copio sp in fp, in modo da sapere dove comincia questo record: � l'AL.
						// Sotto di lui sono presenti i parametri, disposti dal chiamante.
				"lra\n" + // Inserisco sullo stack il return address
				deccode + 
				body.codeGeneration() + 
				// Distruzione dell'AR da qui:
				"srv\n" + // Salvo nel registro rv il risultato della funzione
				popdec + 
				"sra\n" + // Salvo nel registro ra il return address
				"pop\n" + // Rimuovo l'AL
				poppar + 
				"sfp\n" + // Aggiorno il fp con quello del chiamante  
				"lrv\n" + // Inserisco sullo stack il return value
				"lra\n" + // Inserisco sullo stack il return address
				"js\n" // Aggiorno il return address con l'instruction pointer corrente (necessari per ritornare dalla funzione)
						// e l'instruction pointer diventa il return address presente sulla cima dello stack
						// (salto all'indirizzo sulla cima dello stack)
		);

		return "lfp\n" + "push " + funl + "\n";
	}

	@Override
	public Node getSymType() {
		return symType;
	}
	
	public void addSymType(ArrowTypeNode n) {
		symType = n;
	}

}