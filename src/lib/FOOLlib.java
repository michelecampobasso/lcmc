package lib;

import java.util.ArrayList;
import java.util.HashMap;

import ast.*;

public class FOOLlib {

	private static int labCount = 0;

	private static int funLabCount = 0;

	private static String funCode = "";

	private static HashMap<String, String> superTypes;

	/*
	 *  valuta se il tipo "a" � <= al tipo "b", dove "a" e "b" sono tipi di
	 *  base: int o bool
	 */
	public static boolean isSubtype(Node a, Node b) {
		if ((a instanceof BoolTypeNode) && ((b instanceof IntTypeNode) || (b instanceof BoolTypeNode)))
			return true;
		else if ((a instanceof IntTypeNode) && (b instanceof IntTypeNode))
			return true;
		/* 
		 * Estensione Higher-order:
		 * i tipi funzionali sono messi in relazione attraverso la covarianza
		 * sul tipo di ritorno e la controvarianza sul tipo dei parametri.
		 */
		else if (a instanceof ArrowTypeNode && (b instanceof ArrowTypeNode)) {
			ArrowTypeNode c = (ArrowTypeNode) a;
			ArrowTypeNode d = (ArrowTypeNode) b;
			/*
			 * Il tipo di una funzione f1 � sottotipo del tipo di una funzione
			 * f2 se il tipo ritornato da f1 � sottotipo del tipo ritornato da
			 * f2, se hanno il medesimo numero di parametri, e se ogni tipo di
			 * paramentro di f1 � sopratipo del corrisponde tipo di parametro di
			 * f2.
			 */
			if (isSubtype(c.getRet(), d.getRet()) && c.getParList().size() == d.getParList().size()) {
				for (int i = 0; i < c.getParList().size(); i++) {
					// Controvarianza controllata qui
					if (!isSubtype(d.getParList().get(i), c.getParList().get(i)))
						return false;
				}
				return true;
			}
			
		} else if (a instanceof EmptyTypeNode && b instanceof ClassTypeNode
					|| a instanceof ClassTypeNode && b instanceof EmptyTypeNode)
			return true;
		
		else if (a instanceof ClassTypeNode && b instanceof ClassTypeNode) {
			ClassTypeNode c = (ClassTypeNode) a;
			ClassTypeNode d = (ClassTypeNode) b;
			if (c.getId().equals(d.getId()))
				return true;
			if (superTypes.get(c.getId()) != null)
				if (superTypes.get(c.getId()).equalsIgnoreCase(d.getId()))
					return true;
		}
		return false;
	}

	public static String freshLabel() {
		return "label" + (labCount++);
	}

	public static String freshFunLabel() {
		return "function" + (funLabCount++);
	}

	public static void putCode(String c) {
		funCode += "\n" + c; // aggiunge una linea vuota di separazione prima di
								// funzione
	}

	public static String getCode() {
		return funCode;
	}

	public static void putSuperType(HashMap<String, String> supertypes) {
		superTypes = supertypes;
	}

	public static Node lowestCommonAncestor(Node a, Node b) {

		/*
		 * Se sono di tipo classe, delego la ricerca al metodo catchTheAncestor,
		 * mentre se solo uno dei due lo � e l'altro � un null, allora ritorno
		 * il classType
		 */
		if (a instanceof ClassTypeNode && b instanceof ClassTypeNode) {
			Node ancestor;
			if ((ancestor = catchTheAncestor((ClassTypeNode) a, (ClassTypeNode) b)) == null) 
				/*
				 * Se questo controllo fallisce, provo a vedere se invertendoli
				 * esiste un common ancestor. In ogni caso, ritorno il risultato,
				 * positivo o negativo che sia.
				 */
				return catchTheAncestor((ClassTypeNode) b, (ClassTypeNode) a); 
			return ancestor; // Ritorno quello che ho trovato testando a,b
		}
		
		if (a instanceof ClassTypeNode && b instanceof EmptyTypeNode)
			return a;
		if (a instanceof EmptyTypeNode && b instanceof ClassTypeNode)
			return b;

		// Nel caso si tratti di due funzionali...
		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) { 
			ArrowTypeNode c = (ArrowTypeNode) a;
			ArrowTypeNode d = (ArrowTypeNode) b;
			// ...e se hanno lo stesso numero di parametri...
			if (c.getParList().size() == d.getParList().size()) {
				Node returnCommonAncestor;
				ArrayList<Node> parSubTypes = new ArrayList<Node>();
				ArrayList<Node> cParList = c.getParList();
				ArrayList<Node> dParList = d.getParList();
				// ...allora controllo se i return type hanno un common ancestor
				if ((returnCommonAncestor = lowestCommonAncestor(c.getRet(), d.getRet())) == null) 
					return null;
				for (int i = 0; i < cParList.size(); i++) {
					/*
					 * Verificato ceh lo abbiano, controllo che i parametri siano
					 * sottotipi di quelli di b...
					 */
					if (isSubtype(cParList.get(i), dParList.get(i))) 
						// ...e li aggiungo ad una lista per la creazione dell'ArrowType nel caso vada tutto bene
						parSubTypes.add(cParList.get(i)); 
					else 
						// ...altrimenti controllo che il parametro i-esimo di b sia sottotipo di a...
						if (isSubtype(dParList.get(i), cParList.get(i))) 
						parSubTypes.add(dParList.get(i)); // ...e lo aggiungo alla lista
					else
						return null; // il parametro i-esimo di a non � sottotipo di quello di b
				}

				// I parametri sono sottotipi ed i ritorni hanno un common ancestor
				return new ArrowTypeNode(parSubTypes, returnCommonAncestor); 
			} else
				return null; // Numero di parametri diverso
		}

		/*
		 * Se a � intero e b � o un intero o un bool, o se b � intero e a �
		 * bool, torno intero...
		 */
		if (a instanceof IntTypeNode && (b instanceof IntTypeNode || b instanceof BoolTypeNode)
				|| (b instanceof IntTypeNode && a instanceof BoolTypeNode))
			return new IntTypeNode();
		/*
		 * ...mentre se entrambi sono bool, torno bool.
		 */
		if (a instanceof BoolTypeNode && b instanceof BoolTypeNode)
			return new BoolTypeNode();

		return null; // Nessun common ancestor individuato
	}

	private static Node catchTheAncestor(ClassTypeNode a, ClassTypeNode b) {
		while (true) {
			if (superTypes.get(a.getId()) != null) { 
				// La classe a ha un supertipo e creo un ClassTypeNode da controllare
				ClassTypeNode aSuperType = new ClassTypeNode(superTypes.get(a.getId())); 
				// Se b � sottotipo del supertipo, allora questo � il lowestCommonAncestor
				if (isSubtype(b, aSuperType))
					return aSuperType;
				a = aSuperType; // Diversamente, continuo a cercare
			} else // La classe a non ha un supertipo
				break;
		}
		return null;
	}
}